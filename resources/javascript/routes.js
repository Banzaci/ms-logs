import Route from 'react-router';
import App from 'components/app';
import Dashboard from 'components/dashboard';
import Logs from 'components/logs';

export default (
    <Route path="/" component={ App }>
        <Route component={ Dashboard }>
            <IndexRoute component={ Logs }/>
        </Route>
    </Route>
);

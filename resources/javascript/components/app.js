import React from 'react';
import ReactDom from 'react-dom';
import HeaderList from './headers/header-list';
import Logs from './logs/log-list';
import Template from './app-template';
import { Router, Route, IndexRoute, hashHistory } from 'react-router';

export default () => {
    return (
        <Router history={ hashHistory }>
            <Route path="/" component={ Template }>
                <IndexRoute component={ HeaderList } />
                <Route path="logs/:mc" component={ Logs }/>
            </Route>
        </Router>
    );
};

import React from 'react';

export default class List extends React.Component {

    render() {
        const ItemType = this.props.itemType;
        const items = this.props.items || [];
        const markupItems = this.createItemsMarkup(items, ItemType);

        return (<ul>{markupItems}</ul>);
    }

    createItemsMarkup(items, Type) {
        return items.map((item) => {
            return (
                <li key={ item.id }>
                    <Type data={ item } />
                </li>
            );
        });
    }
}

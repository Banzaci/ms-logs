import React from 'react';
import ReactDom from 'react-dom';
import { Link } from 'react-router';

export default class Header extends React.Component {

    render() {

        const data = this.props.data;
        const header = data.microservice;
        const url = `/logs/${header}`;

        return (
            <Link to={url} className="btn btn-success">{ header }</Link>
        );
    }

}

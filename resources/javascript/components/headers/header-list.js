import React from 'react';
import ReactDom from 'react-dom';
import Header from './header';
import List from '../common/list';

export default class HeaderList extends React.Component {

  constructor(props) {
      super(props);
      this.state = { headers: [] };
  }

    render() {
      return (
          <section>
              <header><h3>Microservices</h3></header>
              <section>
                    <List items={ this.state.headers } itemType={ Header }/>
              </section>
          </section>
      );
    }

    componentDidMount() {
        fetch('http://localhost:7000/microservices').then((response) => {
            return response.json();
        }).then((data) => {
            this.setState({ headers:data });
        }).catch((err) => {
            throw new Error(err);
        });
    }
}

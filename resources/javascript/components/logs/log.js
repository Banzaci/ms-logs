import React from 'react';
import ReactDom from 'react-dom';

export default class Log extends React.Component {

    render() {
        const data  = this.props.data;
        const log   = data.message;

        return (
            <div>
                { log }
            </div>
        );
    }

}

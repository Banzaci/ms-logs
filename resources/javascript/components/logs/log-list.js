import React from 'react';
import ReactDom from 'react-dom';
import Log from './log';
import List from '../common/list';
import { Link } from 'react-router';

export default class LogList extends React.Component {

  constructor(props) {
      super(props);
      this.state = { items: [] };
      this.mc = props.params.mc;
  }

    render() {
        return (
            <section>
                <header>
                <Link to="/" className="btn btn-success">
                    Back to list
                </Link>
                <h3>{this.mc}</h3>
                </header>
                <section>
                    <List items={ this.state.items } itemType={ Log }/>
                </section>
            </section>
        );
    }

    componentDidMount() {
        fetch(`http://localhost:7000/logs/${this.mc}`).then((response) => {
            return response.json();
        }).then((data) => {
            this.setState({ items:data.logs });
        }).catch((err) => {
            throw new Error(err);
        });
    }
}

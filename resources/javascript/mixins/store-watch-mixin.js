import React from 'react';
import Request from '../api/request';

export default ( InnerComponent, stateCallback ) => class extends React.Component {
    constructor( props ) {
        super( props );
        this.state = stateCallback( props );
    }
    componentWillMount() {
        Request('GET', 'http://localhost:7000/microservices', (result)=> {
            console.log(result);
            this.setState(result);
        }, (error)=> console.log(error));
    }

    render(){
        return <InnerComponent {...this.state } {...this.props} />
    }
};

import React from 'react';

class Header extends React.Component {
    goto(){
        console.log(this.props.url);
    }
    render() {
        return (React.createElement('button', {onClick:this.goto}, this.props.name));
    }
}

module.exports = Header;

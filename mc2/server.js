import Hapi from 'hapi';
import Wreck from 'wreck';
import Inert from 'inert';
import Vision from 'vision';
import Boom from 'boom';
import Path from 'path';
import Header from '../shared/components/header';
import React from 'react';
import ReactDOMServer from 'react-dom/server';

const port = 5000;
const host = 'localhost';
const logs = [];
const microservice = `Server-2-on-${port}`;

const server = new Hapi.Server();

server.register([ Vision, Inert ], err => {
  if (err) { return console.error(err); }

  server.connection({
      host,
      port
  });

  server.route({
      method: 'GET',
      path: '/microservice',
      handler: function ( request, reply ) {
          logs.push(
              {
                  id:new Date(),
                  message:'Get MS page.'
              }
          );
          reply(
              {
                  microservice,
                  id:port,
                  host
              }
          ).header('x-version', '2.0');
      }
  });

  server.route({
      method: 'GET',
      path: '/logs',
      handler: function ( request, reply ) {
          logs.push(
              {
                  id:new Date(),
                  message:'Get Logs page.'
              }
          );
          reply(
              {
                  logs
              }
          ).header('x-version', '2.0');
      }
  });

  server.start(() => {
      logs.push(
          {
              id:new Date(),
              message:`running at ${server.info.uri}`
          }
      );
      console.log(`running at ${server.info.uri}`);
  });
});


const shutdown = ()=> {
    server.stop(() => console.log(`shutdown at ${server.info.uri}`));
};

process
  .once('SIGINT', shutdown)
  .once('SIGTERM', shutdown);

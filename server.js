import Bcrypt from 'bcrypt';
import Hapi from 'hapi';
import Basic from 'hapi-auth-basic';
import once from 'once';
import Wreck from 'wreck';
import Inert from 'inert';
import Vision from 'vision';
import Boom from 'boom';
import H2O2 from 'h2o2';
import Path from 'path';

const server = new Hapi.Server({
    connections: {
        routes: {
            files: {
                relativeTo: Path.join(__dirname, 'public')
            }
        }
    }
});

server.connection({
    host: 'localhost',
    port: 7000
});

const proxyMap = {
    '/logs/Server-1-on-3000': 'http://localhost:3000/logs',
    '/logs/Server-2-on-5000': 'http://localhost:5000/logs'
};

const proxyHandler = {
      proxy: {
          mapUri: (req, cb) => {
              const uri = proxyMap[req.path];
              if (!uri) { return cb(Boom.notFound()); }
              cb(null, uri, req.headers);
          },
          onResponse: (err, res, req, reply, settings, ttl) => {
              if (err) { return reply(err); }
              reply(res);
          }
      }
};

const users = {
    john: {
        username: 'john',
        password: '$2a$10$iqJSHD.BGr0E2IxQwYgJmeP3NvhPrXAeLSaGCj6IR/XU5QtjVu5Tm',   // 'secret'
        name: 'John Doe',
        id: '2133d32a'
    }
};

const validate = function (request, username, password, callback) {
    const user = users[username];
    if (!user) {
        return callback(null, false);
    }
    console.log(user);
    Bcrypt.compare(password, user.password, (err, isValid) => {
        callback(err, isValid, { id: user.id, name: user.name });
    });
};

server.register([{register: H2O2}, Vision, Inert, Basic ], err => {
    if (err) return console.error(err);

    server.auth.strategy('simple', 'basic', { validateFunc: validate });

    server.views({
        engines: {
          ejs: require('ejs')
        },
        relativeTo: __dirname,
        path: 'views'
    });

    server.route({
        method: 'GET',
        path: '/',
        config: {
            auth: 'simple',
            handler (request, reply) {
                reply.view('index');
                //reply('hello, ' + request.auth.credentials.name);
            }
        }
    });

    server.route([
      { method: 'GET', path: '/logs/Server-1-on-3000', handler: proxyHandler },
      { method: 'GET', path: '/logs/Server-2-on-5000', handler: proxyHandler }
    ]);

    server.route([
      { method: 'GET', path: '/microservices', handler: ( req, reply ) => {
          reply = once(reply);
          Wreck.get('http://localhost:3000/microservice', { json:true },
              (err, res, mc) => next(err, mc));

          Wreck.get('http://localhost:5000/microservice', { json:true } ,
              (err, res, mc) => next(err, mc));

          const results = [];
          function next(err, result){
              if(err) return reply( err );
              results.push(result);
              if(results.length === 2){
                  return reply(results);
              }
          }
      }}
    ]);

    server.route({
        method: 'GET',
        path: '/javascript/{filename}',
        handler(request, reply) {
            reply.file('javascript/'+request.params.filename);
        }
    });

    server.start(() => {
        console.log(`running at ${server.info.uri}`);
    });

});

const shutdown = () => {
    server.stop(() => console.log(`shutdown at ${server.info.uri}`));
};

process
  .once('SIGINT', shutdown)
  .once('SIGTERM', shutdown);
